# Week8MiniProject


## Overview
This project is a Rust command-line tool designed to select a random fruit from a predefined list. It showcases basic Rust development practices, including data ingestion from a static array, command-line interface creation, and unit testing.

## Installation
1. Clone the project from the week4 miniproject repository

2. Add dependencies to Cargo.toml

```bash
clap = "~2.27.0"
```

This project uses clap for command-line argument parsing.

3. Modify main.rs to implement CLI functionality

## Building the Project

1. Build the project with Cargo
```bash
cargo build
```

2. Run the project
```bash
cargo run
```

![Running the project](./Week8_cargo_run.png)

## Testing
Execute unit tests with
```bash
cargo test
```

![Unit tests output](./Week8_cargo_test.png)

## Dockerization
1. Build a Docker image for the project
```bash
docker build -t myimage .
```

![Docker build output](./Week8_docker_build.png)

2. Run the Docker Container
```bash
docker run --rm myimage
```
![Docker build output](./Week8_docker_run.png)