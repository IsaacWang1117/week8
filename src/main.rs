//use clap::{App, Arg};
use clap::App;
use rust_axum_random_fruit_microservice::random_fruit;
//use std::process;

fn main() {
    let _matches = App::new("Random Fruit CLI")
        .version("1.0")
        .author("Isaac")
        .about("Provides a random fruit name")
        .get_matches();

    // Since this tool provides a random fruit and does not require input,
    // there's no need for argument parsing beyond the basic setup.

    // Call the random_fruit function and print the result.
    let fruit = random_fruit();
    println!("Your random fruit is: {}", fruit);
}
